public class Game {
    public static String winner(String[] deckSteve, String[] deckJosh) {
        int steveScore = 0;
        int joshScore = 0;
        
        String cardRank = "23456789TJQKA";

        for (int i = 0; i < deckSteve.length; i++) {
            char steveCard = deckSteve[i].charAt(0);
            char joshCard = deckJosh[i].charAt(0);
            
            int steveCardValue = cardRank.indexOf(steveCard);
            int joshCardValue = cardRank.indexOf(joshCard);

            if (steveCardValue > joshCardValue) {
                steveScore++;
            } else if (steveCardValue < joshCardValue) {
                joshScore++;
            }
        }

        if (steveScore > joshScore) {
            return "Steve wins " + steveScore + " to " + joshScore;
        } else if (joshScore > steveScore) {
            return "Josh wins " + joshScore + " to " + steveScore;
        } else {
            return "Tie";
        }
    }
}
