public class Kata {
  
    public static String solution(String str) {
      if (str == null || str.length() == 0) {
        return str;
      }
      StringBuilder sb = new StringBuilder(str);
      return sb.reverse().toString();
    }
  }